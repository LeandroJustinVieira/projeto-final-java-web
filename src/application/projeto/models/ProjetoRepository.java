package application.projeto.models;

import java.util.ArrayList;
import java.util.List;

public class ProjetoRepository {

    private static List<Projeto> projetos = new ArrayList<>();

    public static void salvar(Projeto projeto) {

        if(!ProjetoRepository.projetos.contains(projeto)) {
            ProjetoRepository.projetos.add(projeto);
        }
    }

    public static void excluir(Projeto projeto) {
        ProjetoRepository.projetos.remove(projeto);
    }

    public static Projeto getProjeto(Integer codigo) {

        return ProjetoRepository.projetos.stream()
                .filter(projeto -> projeto.getCodigo().equals(codigo)).findFirst().orElse(null);
    }

    public static List<Projeto> getProjetos() {
        return ProjetoRepository.projetos;
    }



}
