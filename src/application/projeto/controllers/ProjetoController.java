package application.projeto.controllers;


import application.projeto.models.Projeto;
import application.projeto.models.ProjetoRepository;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class ProjetoController {

    private Projeto projeto;
    private List<Projeto> projetos = new ArrayList<>();

    public ProjetoController() {
        novo();
        atualizarListaProjetos();
    }

    public void novo() {
        projeto = new Projeto();
    }

    private void atualizarListaProjetos() {
        projetos = ProjetoRepository.getProjetos();
    }

    public void salvar() {
        ProjetoRepository.salvar(projeto);
        this.atualizarListaProjetos();
        this.novo();
    }

    public void excluir(Projeto projeto) {
        ProjetoRepository.excluir(projeto);
        this.atualizarListaProjetos();
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Projeto> projetos) {
        this.projetos = projetos;
    }
}
