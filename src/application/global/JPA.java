package application.global;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPA {

    public static EntityManagerFactory emf;

    public static EntityManager getEM() {
        if(emf == null) {
            emf = Persistence.createEntityManagerFactory("default");
        }
        return emf.createEntityManager();
    }


}
