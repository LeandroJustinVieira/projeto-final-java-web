package application.atividade.models;

import application.projeto.models.Projeto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AtividadeRepository {

    private static List<Atividade> atividades = new ArrayList<>();

    public static void salvar(Atividade atividade) {
        if(!AtividadeRepository.atividades.contains(atividade)) {
            AtividadeRepository.atividades.add(atividade);

        }
    }

    public static void excluir(Atividade atividade) {
        AtividadeRepository.atividades.remove(atividade);
    }

    public static Atividade getAtividade(Integer codigo) {
        return AtividadeRepository.atividades.stream()
                .filter(atividade -> atividade.getCodigo().equals(codigo)).findFirst().orElse(null);
    }

    public static List<Atividade> getAtividadesByProjeto(Integer projeto) {

        return AtividadeRepository.atividades.stream().filter(atividade -> atividade.getProjeto()
                .getCodigo().equals(projeto)).collect(Collectors.toList());
    }

    public static List<Atividade> getAtividades() {
        return AtividadeRepository.atividades;
    }

}
