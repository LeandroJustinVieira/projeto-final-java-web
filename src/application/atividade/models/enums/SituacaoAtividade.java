package application.atividade.models.enums;

public enum SituacaoAtividade {

    NAO_INICIALIZADA("Não inicializada"),
    INICIALIZADA("Inicializada"),
    ENCERRADA("Encerrada");

    String nome;

    SituacaoAtividade(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
