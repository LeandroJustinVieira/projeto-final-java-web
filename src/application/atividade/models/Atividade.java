package application.atividade.models;

import application.atividade.models.enums.SituacaoAtividade;
import application.projeto.models.Projeto;

import java.util.Objects;

public class Atividade {

    private Integer codigo;
    private String nome;
    private Double quantidadeEsforco;
    private Double quantidadeTempoGasto;
    private SituacaoAtividade situacaoAtividade;
    private Projeto projeto;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getQuantidadeEsforco() {
        return quantidadeEsforco;
    }

    public void setQuantidadeEsforco(Double quantidadeEsforco) {
        this.quantidadeEsforco = quantidadeEsforco;
    }

    public Double getQuantidadeTempoGasto() {
        return quantidadeTempoGasto;
    }

    public void setQuantidadeTempoGasto(Double quantidadeTempoGasto) {
        this.quantidadeTempoGasto = quantidadeTempoGasto;
    }

    public SituacaoAtividade getSituacaoAtividade() {
        return situacaoAtividade;
    }

    public void setSituacaoAtividade(SituacaoAtividade situacaoAtividade) {
        this.situacaoAtividade = situacaoAtividade;
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getCodigo());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Atividade)) {
            return false;
        }
        final Atividade other = (Atividade) obj;

        return Objects.equals(this.codigo, other.codigo);
    }


}
