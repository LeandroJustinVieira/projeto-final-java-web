package application.atividade.controllers;

import application.atividade.models.Atividade;
import application.atividade.models.AtividadeRepository;
import application.atividade.models.enums.SituacaoAtividade;
import application.projeto.models.Projeto;
import application.projeto.models.ProjetoRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class AtividadeController {

    private Integer codigoProjeto;
    private Projeto projeto;
    private Atividade atividade;
    private List<Atividade> atividades = new ArrayList<>();

    public AtividadeController() {
        this.projeto = new Projeto();
    }

    @PostConstruct
    public void init() {
        codigoProjeto = Integer.valueOf(
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("codigo")
        );
        this.atualizarListaAtividades();
        this.projeto = ProjetoRepository.getProjeto(codigoProjeto);
        this.novo();
    }

    public void novo() {
        atividade = new Atividade();
        atividade.setProjeto(projeto);
    }

    private void atualizarListaAtividades() {
        atividades = AtividadeRepository.getAtividadesByProjeto(codigoProjeto);
//        atividades = AtividadeRepository.getAtividades();
    }

    public void salvar() {
        AtividadeRepository.salvar(atividade);
        this.atualizarListaAtividades();
        this.novo();
    }

    public void excluir(Atividade atividade) {
        AtividadeRepository.excluir(atividade);
        this.atualizarListaAtividades();
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public SituacaoAtividade[] getSituacoesAtividade(){
        return SituacaoAtividade.values();
    }

    public Atividade getAtividade() {
        return atividade;
    }

    public void setAtividade(Atividade atividade) {
        this.atividade = atividade;
    }

    public List<Atividade> getAtividades() {
        return atividades;
    }

    public void setAtividades(List<Atividade> atividades) {
        this.atividades = atividades;
    }
}

